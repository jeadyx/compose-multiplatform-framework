import androidx.compose.animation.AnimatedVisibility
import androidx.compose.material.Typography
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.platform.Font
import androidx.compose.ui.window.CanvasBasedWindow
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.resource

@OptIn(ExperimentalComposeUiApi::class)
fun main() {
    CanvasBasedWindow(canvasElementId = "ComposeTarget") {
        var typography by remember { mutableStateOf<Typography?>(null) }
        var fontReady by remember { mutableStateOf(false) }
        LaunchedEffect(Unit) {
            val font = loadCjkFont()
            Typography(defaultFontFamily = font).also {
                typography = it
            }
            fontReady = true
        }
        if(fontReady){
            App(typography)
        }
    }
}

@OptIn(ExperimentalResourceApi::class)
suspend fun loadCjkFont(): FontFamily {
    val regular = resource("font/fangzhenglantingqianhei.ttf").readBytes()

    return FontFamily(
        Font(identity = "maoRegular", data = regular, weight = FontWeight.Normal),
    )
}